<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Nourse - App</title>

  <!-- Custom fonts for this template-->
  <link href="<c:url value="/resources/css/all.min.css"/>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<c:url value="/resources/css/sb-admin-2.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/bootstrap-datepicker.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/valid.css"/>" rel="stylesheet">
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
        	<a href="<c:url value="/"/>" class="btn btn-info btn-icon-split" style="position: absolute; top: 0px; right: 0px;">
                    <span class="text">Return</span>
                  </a>
          <div class="col-lg-3 d-none d-lg-block"></div>
          <div class="col-lg-6">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Add Patient</h1>
              </div>
              
              <c:choose>
              	<c:when test="${not empty successMsg}">
              		<div class="form-group row">
		                  <div class="col-sm-12 mb-3"style="padding: 0">
			                    <a href="#" class="btn btn-success  btn-icon-split" style="width: 100%">
				                    <span class="icon text-white-50 pull-left" style="position: absolute; left: 13px;">
				                      <i class="fas fa-exclamation-triangle"></i>
				                    </span>
				                    <span class="text">Saved Data</span>
				                  </a>
		                  </div>
		                  <a href="<c:url value="/home"/>" class="btn btn-primary btn-user btn-block mone-field">
			                  Return
			                </a>
		                </div>
              	</c:when>
              	<c:otherwise>
              		<c:url var="url" value="/add/patient"/>
		              <sf:form class="user" modelAttribute="patientModel" action="${url}" method="POST" id="formVal">
		              	 <div class="form-group row">
		                  <div class="col-sm-12 mb-3 mb-sm-0">
		                    <c:if test="${not empty errorMessage}">
			                    <a href="#" class="btn btn-danger btn-icon-split" style="width: 100%">
				                    <span class="icon text-white-50 pull-left" style="position: absolute; left: 13px;">
				                      <i class="fas fa-exclamation-triangle"></i>
				                    </span>
				                    <span class="text">${errorMessage}</span>
				                  </a>
			                  </c:if>
		                  </div>
		                </div>
		              	 
		                <div class="form-group row">
		                  <div class="col-sm-12 mb-3 mb-sm-0">
		                    <sf:input path="name" type="text" cssClass="form-control form-control-user" id="exampleFirstName" placeholder="Name" data-validation="required" data-validation-error-msg-required="Required"/>
		                  </div>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12">
		                    <sf:input path="lastName" type="text" cssClass="form-control form-control-user" id="exampleLastName" placeholder="Last Name" data-validation="required" data-validation-error-msg-required="Required"/>
		                  </div>
		                </div>
		                <div class="form-group">
		                  <sf:input path="ci" type="number" cssClass="form-control form-control-user" id="exampleInputEmail" placeholder="CI" data-validation="required" data-validation-error-msg-required="Required"/>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12 mb-3 mb-sm-0">
		                    <sf:input path="dateBirth" autocomplete="off" type="text" cssClass="form-control datepicker form-control-user" id="exampleInputPassword" placeholder="Date of Birth" data-validation="required" data-validation-error-msg-required="Required"/>
		                  </div>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12">
		                    <sf:input path="phoneNumber" type="number" cssClass="form-control form-control-user" id="exampleRepeatPassword" placeholder="Phone Number" data-validation="required" data-validation-error-msg-required="Required"/>
		                  </div>
		                </div>
		                <hr>
		                <button type="button" class="btn btn-primary btn-user btn-block" id="next">
		                  Next
		                </button>
		                   
		                <c:forEach items="${patientModel.listSigns}" var="sl" varStatus="loop">
		                	<div class="form-group row mone-field" style="display: none">
			                  <div class="col-sm-12">
			                  	<sf:input path="listSigns[${loop.index}].signsBean.idSign" type="hidden"/>
			                  	<sf:input path="listSigns[${loop.index}].signsBean.minValue" type="hidden"/>
			                  	<sf:input path="listSigns[${loop.index}].signsBean.maxValue" type="hidden"/>
			                  	
			                  	<c:choose>
			                  		<c:when test="${sl.signsBean.maxValue != 0.0 && sl.signsBean.minValue != 0.0}">
			                  			<sf:input path="listSigns[${loop.index}].value" type="number" class="form-control form-control-user" placeholder="${sl.signsBean.description}"/>
			                  		</c:when>
			                  		<c:otherwise>
			                  			<sf:input path="listSigns[${loop.index}].value" type="text" class="form-control form-control-user" placeholder="${sl.signsBean.description}"/>
			                  		</c:otherwise>
			                  	</c:choose>
			                  	
			                  </div>
			                </div>
		                </c:forEach>
		                
		                <div class="form-group row mone-field" style="display: none">
		                  <div class="col-sm-12">
		                    <sf:input path="infoAditional" type="text" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Informacion Adicional"/>
		                  </div>
		                </div>
		                
		                <button type="submit" class="btn btn-primary btn-user btn-block mone-field" id="finish" style="display: none">
		                  Add
		                </button>
		              </sf:form>
              	</c:otherwise>
              </c:choose>
              
<!--               <hr> -->
<!--               <div class="text-center"> -->
<!--                 <a class="small" href="forgot-password.html">Forgot Password?</a> -->
<!--               </div> -->
<!--               <div class="text-center"> -->
<!--                 <a class="small" href="login.html">Already have an account? Login!</a> -->
<!--               </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap.bundle.min.js"/>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<c:url value="/resources/js/jquery.easing.min.js"/>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<c:url value="/resources/js/sb-admin-2.min.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap-datepicker.es.min.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap-datepicker.min.js"/>"></script>
  <script src="<c:url value="/resources/js/jquery.form-validator.min.js"/>"></script>

<script>
 $("#next").on('click', function(){
	 $(this).slideUp();
	 $(".mone-field").slideDown();
 })
 
 $('.datepicker').datepicker({
	 autoclose: true,
     todayHighlight: true,
     format: 'dd/mm/yyyy',
     endDate: '0d',
     language: 'es',
     orientation: 'bottom right'
  })
  
  $.validate({
         form: '#formVal',
         errorElementClass: 'validation-error',
         errorMessageClass: 'input-error',
         borderColorOnError: '',
         validateOnBlur: true,
         scrollToTopOnError: false
         /*onError: function () {
             $('.global-spinner-container').hide();
         }*/
     })
</script>

</body>

</html>
