<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Nourse - App</title>

  <!-- Custom fonts for this template-->
  <link href="<c:url value="/resources/css/all.min.css"/>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<c:url value="/resources/css/sb-admin-2.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/bootstrap-datepicker.min.css"/>" rel="stylesheet">
  <link href="<c:url value="/resources/css/valid.css"/>" rel="stylesheet">
</head>

<body class="bg-gradient-primary">

	

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
        <a href="<c:url value="/home"/>" class="btn btn-info btn-icon-split" style="position: absolute; top: 0px; right: 0px;">
                    <span class="text">Return</span>
                  </a>
          <div class="col-lg-3 d-none d-lg-block"></div>
          <div class="col-lg-6">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Patient Data</h1>
              </div>
              
              
              
              		  <c:url var="url" value="/patient/edit"/>
              		  <c:url var="url2" value="/patient/delete"/>
		              <form class="user" action="${url}" method="POST" id="editForm">
		              	<input type="hidden" name="id" value="${patientData.idPatient}"/>	
		              </form>	
		              <form class="user" action="${url2}" method="POST" id="deleteForm">
		              	<input type="hidden" name="id" value="${patientData.idPatient}"/>	
		              </form>
		              <form class="user" action="${url}" method="POST">
		              	<input type="hidden" name="id" value="${patientData.idPatient}"/>	
		              </form>	              	 
		                <div class="form-group row">
		                  <div class="col-sm-12 mb-3 mb-sm-0">
		                  	<label class="font-weight-bold text-primary">Name : </label>
		                  	<span>${patientData.name}</span>
		                  </div>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12">
		                    <label class="font-weight-bold text-primary">Last Name : </label>
		                  	<span>${patientData.lastname}</span>
		                  </div>
		                </div>
		                <div class="form-group">
		                  <label class="font-weight-bold text-primary">CI : </label>
		                  <span>${patientData.ci}</span>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12 mb-3 mb-sm-0">
		                    <label class="font-weight-bold text-primary">Date of Birth : </label>
		                  	<span><fmt:formatDate value="${patientData.dateOfBirth}" pattern="dd/MM/yyyy"/></span>
		                  </div>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12">
		                    <label class="font-weight-bold text-primary">Age : </label>
		                  	<span>${patientData.age}</span>
		                  </div>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12">
		                    <label class="font-weight-bold text-primary">Phone Number : </label>
		                  	<span>${patientData.phoneNumber}</span>
		                  </div>
		                </div>
		                <div class="form-group row">
		                  <div class="col-sm-12">
		                    <label class="font-weight-bold text-primary">Aditional Info : </label>
		                  	<span>${patientData.aditionalInfo}</span>
		                  </div>
		                </div>
		                
		                <hr>
		                
		                <c:forEach items="${patientData.signs}" var="sl" varStatus="loop">
		                	<div class="form-group row mone-field">
			                  <div class="col-sm-12">
			                  	<c:choose>
			                  		<c:when test="${not empty sl.signsBean.maxValue && not empty sl.signsBean.minValue}">
			                  			<c:choose>
					                  		<c:when test="${sl.valueD > sl.signsBean.minValue && sl.valueD < sl.signsBean.maxValue}">
					                  			<a href="#" class="btn btn-success btn-icon-split">
								                    <span class="icon text-white-50">
								                      <i class="fas fa-check"></i>
								                    </span>
								                    <span class="text">${sl.signsBean.description}</span><br>
								                    <span class="text">${sl.value}</span>
								                  </a><span class="font-weight-bold text-primary">&nbsp;${sl.signsBean.minValue} - ${sl.signsBean.maxValue}</span>
					                  		</c:when>
					                  		<c:when test="${sl.valueD < sl.signsBean.minValue}">
					                  			<a href="#" class="btn btn-warning btn-icon-split">
								                    <span class="icon text-white-50">
								                      <i class="fas fa-exclamation-triangle"></i>
								                    </span>
								                    <span class="text">${sl.signsBean.description}</span><br>
								                    <span class="text">${sl.value}</span>
								                  </a><span class="font-weight-bold text-primary">&nbsp;${sl.signsBean.minValue} - ${sl.signsBean.maxValue}</span>
					                  		</c:when>
					                  		<c:otherwise>
					                  			<a href="#" class="btn btn-warning btn-icon-split">
								                    <span class="icon text-white-50">
								                      <i class="fas fa-exclamation-triangle"></i>
								                    </span>
								                    <span class="text">${sl.signsBean.description}</span><br>
								                    <span class="text">${sl.value}</span>
								                  </a><span class="font-weight-bold text-primary">&nbsp;${sl.signsBean.minValue} - ${sl.signsBean.maxValue}</span>
					                  		</c:otherwise>
					                  	</c:choose>	
			                  		</c:when>
			                  		<c:otherwise>
			                  			<label class="font-weight-bold text-primary">${sl.signsBean.description}: </label>
			                  			<span class="normal">${sl.value}</span>
			                  		</c:otherwise>
			                  	</c:choose>	  		
			                  </div>
			                </div>
		                </c:forEach>
		                 <hr>
		                <button type="button" class="btn btn-primary btn-user btn-block mone-field" id="edit">
		                  Edit
		                </button>
		                <button type="button" class="btn btn-danger btn-user btn-block mone-field" data-toggle="modal" data-target="#myModal">
		                  Delete
		                </button>
              
<!--               <hr> -->
<!--               <div class="text-center"> -->
<!--                 <a class="small" href="forgot-password.html">Forgot Password?</a> -->
<!--               </div> -->
<!--               <div class="text-center"> -->
<!--                 <a class="small" href="login.html">Already have an account? Login!</a> -->
<!--               </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title text-warning">Warning.!!</h4>
          <span class="pull-right" data-dismiss="modal">&times;</span>
        </div>
        <div class="modal-body">
          <p>Do you want to delete patient information?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" id="delet">Delete</button>
          <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>





  <!-- Bootstrap core JavaScript-->
  <script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap.bundle.min.js"/>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<c:url value="/resources/js/jquery.easing.min.js"/>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<c:url value="/resources/js/sb-admin-2.min.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap-datepicker.es.min.js"/>"></script>
  <script src="<c:url value="/resources/js/bootstrap-datepicker.min.js"/>"></script>

<script>
 $("#next").on('click', function(){
	 $(this).slideUp();
	 $(".mone-field").slideDown();
 })
 $("#edit").on('click', function(){
	 $("#editForm").submit();
 })
 $("#delet").on('click', function(){
	 $("#deleteForm").submit();
 })
 
 $('.datepicker').datepicker({
	 autoclose: true,
     todayHighlight: true,
     format: 'dd/mm/yyyy',
     endDate: '0d',
     language: 'es',
     orientation: 'bottom right'
  })
</script>

</body>

</html>
