package com.nouse.app.util;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
public class PropertiesUtils {

    public static final String PROPERTIES_FILE = "nourse.properties";
    public static final String ERRORS_FILE = "nourse.error.properties";
    
    public static final String BD_NAME = "bdName";
    public static final String BD_URL = "bdUrl";
    public static final String BD_USER = "bdUser";
    public static final String BD_PASS = "bdPass";
    public static final String BD_DRIVER = "bdDriver";
    
    /**
     * Las llamadas a este m�todo siempre se deben hacer de la siguiente manera:
     * PropertiesUtils.getProperty(PropertiesUtils.API_KEY_PROP); -> ejemplo para el caso de API_KEY_PROP
     */
    public static String getProperty(String propertyName) {
        Properties props = null;
        try {
            props = PropertiesUtils.getProperties(PROPERTIES_FILE);
            return props.getProperty(propertyName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getError(String errorCode) {
        Properties props = null;
        try {
            props = PropertiesUtils.getProperties(ERRORS_FILE);
            return props.getProperty(errorCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private static Properties getProperties(String propFileName) throws IOException {

        switch (getServerType()) {

            case GlassFish:
                return fromGlssFish(propFileName);

            case Jboss4:
                return fromJboss4(propFileName);

            case Jboss7:
                return fromJobss7(propFileName);

            case Tomcat:
                return fromTomcat(propFileName);

            case None:
                return fromFile(null);
            //throw new IOException("Server is not defined");

            default:
                throw new IOException("Server is not defined");
        }

    }

    private static Properties fromFile(String fileName) throws IOException {
        Properties properties = new Properties();
        String instanceRoot = "c:/tmp/bna/";
        fileName = "bna.app.properties";
        FileReader fr = new FileReader(new File(instanceRoot + File.separator + fileName));
        properties.load(fr);
        return properties;
    }

    private static Properties fromJboss4(String fileName) throws IOException {
        Properties properties = new Properties();
        String instanceRoot = System.getProperty(jboss4InstanceRootPropertyName);
        URL url = new URL(instanceRoot + "/" + fileName);
        properties.load(url.openStream());
        return properties;
    }

    private static Properties fromJobss7(String fileName) throws IOException {
        Properties properties = new Properties();
        String instanceRoot = System.getProperty(jboss7InstanceRootPropertyName);
        FileReader fr = new FileReader(new File(instanceRoot + File.separator + fileName));
        properties.load(fr);
        return properties;
    }

    private static Properties fromGlssFish(String fileName) throws IOException {

        String instanceRoot = System.getProperty(glassfishInstanceRootPropertyName);
        String glassfishDomainConfigurationFolderName = "config";
        File configurationFolder = new File(instanceRoot + File.separator + glassfishDomainConfigurationFolderName);
        File configFile = new File(configurationFolder, fileName);
        Properties properties = new Properties();
        FileReader fr = new FileReader(configFile);
        properties.load(fr);
        fr.close();

        return properties;

    }

    private static Properties fromTomcat(String fileName) throws IOException {
        Properties properties = new Properties();
        String instanceRoot = System.getProperty(tomcatInstanceRootPropertyName);
        FileReader fr = new FileReader(new File(instanceRoot + File.separator + "conf" + File.separator + fileName));
        properties.load(fr);
        return properties;
    }

    private enum ServerType {
        Jboss4,
        Jboss7,
        GlassFish,
        Tomcat,
        None;
    }

    private static String glassfishInstanceRootPropertyName = "com.sun.aas.instanceRoot";
    private static String jboss4InstanceRootPropertyName = "jboss.server.config.url";
    private static String jboss7InstanceRootPropertyName = "jboss.server.config.dir";
    private static String tomcatInstanceRootPropertyName = "catalina.base";


    private static ServerType getServerType() {

        String instanceRoot = System.getProperty(glassfishInstanceRootPropertyName);
        if (instanceRoot != null) {
            return ServerType.GlassFish;
        }

        instanceRoot = System.getProperty(jboss4InstanceRootPropertyName);
        if (instanceRoot != null) {
            return ServerType.Jboss4;
        }

        instanceRoot = System.getProperty(jboss7InstanceRootPropertyName);
        if (instanceRoot != null) {
            return ServerType.Jboss7;
        }

        instanceRoot = System.getProperty(tomcatInstanceRootPropertyName);
        if (instanceRoot != null) {
            return ServerType.Tomcat;
        }
        return ServerType.None;
    }
}
