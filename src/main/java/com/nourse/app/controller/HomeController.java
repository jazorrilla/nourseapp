package com.nourse.app.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nourse.app.bean.NourseData;
import com.nourse.app.bean.Patient;
import com.nourse.app.bean.PatientModel;
import com.nourse.app.bean.SignsBean;
import com.nourse.app.bean.SignsModel;
import com.nourse.app.service.ServicePatient;
import com.nourse.app.service.ServicePatientImpl;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/*@Autowired
	private ServicePatient servicePatient;	*/
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	//@ResponseBody
	public String init(Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		return "redirect:/login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	//@ResponseBody
	public String login(Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		NourseData nourseData = new NourseData();
		
		model.addAttribute("nourseData", nourseData);
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	//@ResponseBody
	public String login(NourseData nourseData, Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		ServicePatientImpl servicePatient = new ServicePatientImpl();
		
		try {
			nourseData = servicePatient.validateNourse(nourseData.getUser(), nourseData.getPass());
		} catch (Exception e) {
			model.addAttribute("errorMessage", e.getMessage());
			model.addAttribute("nourseData", nourseData);
			return "login";
		}
		
		session.setAttribute("nourseData", nourseData);
		return "redirect:/home";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	//@ResponseBody
	public String logout(Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		session.setAttribute("nourseData", null);
		session.invalidate();
		
		return "redirect:/login";
	}
	
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	//@ResponseBody
	public String home(Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		ServicePatientImpl servicePatient = new ServicePatientImpl();
		List<Patient> list = servicePatient.getPatient();
		
		model.addAttribute("patientList", list);
		session.setAttribute("patientList", list);
		return "tables";
	}
	
	@RequestMapping(value = "/patient/{id}", method = RequestMethod.GET)
	//@ResponseBody
	public String patient(@PathVariable("id") String id, Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		/*Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		*/
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
				
		List<Patient> list = (List<Patient>) session.getAttribute("patientList");
		Patient patient = new Patient();
		for(Patient pa : list) {
			if(pa.getIdPatient() == Integer.parseInt(id)) {
				patient = pa;
				break;
			}
		}
		
		model.addAttribute("patientData", patient);
		return "patient_info";
	}
	
	@RequestMapping(value = "/add/patient", method = RequestMethod.GET)
	//@ResponseBody
	public String addPatient(Locale locale, Model model, HttpSession session) {
		
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
				
		ServicePatientImpl servicePatient = new ServicePatientImpl();
		PatientModel patientModel = new PatientModel();
		List<SignsBean> signs = servicePatient.getSigns();
		List<SignsModel> listMSigns = new ArrayList<SignsModel>();
		for(SignsBean sg : signs){
			SignsModel ms= new SignsModel();
			ms.setSignsBean(sg);
			listMSigns.add(ms);
		}
		patientModel.setListSigns(listMSigns);
		//model.addAttribute("signsList", );
		
		model.addAttribute("patientModel", patientModel);
		model.addAttribute("successMsg", null);
		return "patient/add";
	}
	@RequestMapping(value = "/add/patient", method = RequestMethod.POST)
	//@ResponseBody
	public String addPatient(PatientModel patientModel, Locale locale, Model model, HttpSession session) {
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
		
		ServicePatientImpl servicePatient = new ServicePatientImpl();
		try {
			servicePatient.insertPatient(patientModel);
		} catch (Exception e) {
			logger.error("ERROR!! : " + e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "patient/add";
		}
		
		patientModel = new PatientModel();
		List<SignsBean> signs = servicePatient.getSigns();
		List<SignsModel> listMSigns = new ArrayList<SignsModel>();
		for(SignsBean sg : signs){
			SignsModel ms= new SignsModel();
			ms.setSignsBean(sg);
			listMSigns.add(ms);
		}
		patientModel.setListSigns(listMSigns);
		
		model.addAttribute("patientModel", patientModel);
		model.addAttribute("successMsg", "OK");
		return "patient/add";
	}
	
	

	@RequestMapping(value = "/patient/edit", method = RequestMethod.POST)
	//@ResponseBody
	public String patientEdit(String id, Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
		
		PatientModel patientModel = new PatientModel();
		
		List<Patient> list = (List<Patient>) session.getAttribute("patientList");
		Patient patient = new Patient();
		for(Patient pa : list) {
			if(pa.getIdPatient() == Integer.parseInt(id)) {
				patient = pa;
				break;
			}
		}
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		patientModel.setIdPatient(patient.getIdPatient());
		patientModel.setIdPart(patient.getIdPark());
		patientModel.setName(patient.getName());
		patientModel.setLastName(patient.getLastname());
		patientModel.setCi(patient.getCi());
		patientModel.setDateBirth(df.format(patient.getDateOfBirth()));
		patientModel.setPhoneNumber(patient.getPhoneNumber());
		patientModel.setInfoAditional(patient.getAditionalInfo());
		patientModel.setListSigns(patient.getSigns());
		
		model.addAttribute("patientModel", patientModel);
		
		return "edit";
	}
	
	@RequestMapping(value = "/edit/patient", method = RequestMethod.POST)
	//@ResponseBody
	public String editPatient(PatientModel patientModel, Locale locale, Model model, HttpSession session) {
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
		
		ServicePatientImpl servicePatient = new ServicePatientImpl();
		try {
			servicePatient.editPatient(patientModel);
		} catch (Exception e) {
			logger.error("ERROR!! : " + e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return "edit";
		}
		
		patientModel = new PatientModel();
		List<SignsBean> signs = servicePatient.getSigns();
		List<SignsModel> listMSigns = new ArrayList<SignsModel>();
		for(SignsBean sg : signs){
			SignsModel ms= new SignsModel();
			ms.setSignsBean(sg);
			listMSigns.add(ms);
		}
		patientModel.setListSigns(listMSigns);
		
		model.addAttribute("patientModel", patientModel);
		model.addAttribute("successMsg", "OK");
		return "patient/add";
	}
	
	
	@RequestMapping(value = "/patient/delete", method = RequestMethod.POST)
	//@ResponseBody
	public String patientDelet(String id, Locale locale, Model model, HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		if(session.getAttribute("nourseData") == null) {
			return "redirect:/login";
		}
		
		PatientModel patientModel = new PatientModel();
		
		List<Patient> list = (List<Patient>) session.getAttribute("patientList");
		Patient patient = new Patient();
		for(Patient pa : list) {
			if(pa.getIdPatient() == Integer.parseInt(id)) {
				patient = pa;
				break;
			}
		}
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		patientModel.setIdPatient(patient.getIdPatient());
		patientModel.setIdPart(patient.getIdPark());
		patientModel.setName(patient.getName());
		patientModel.setLastName(patient.getLastname());
		patientModel.setCi(patient.getCi());
		patientModel.setDateBirth(df.format(patient.getDateOfBirth()));
		patientModel.setPhoneNumber(patient.getPhoneNumber());
		patientModel.setInfoAditional(patient.getAditionalInfo());
		patientModel.setListSigns(patient.getSigns());
		
		ServicePatientImpl servicePatient = new ServicePatientImpl();
		try {
			servicePatient.deletePatient(patientModel);
			model.addAttribute("successMsg", "OK");
		} catch (Exception e) {
			logger.error("ERROR!! : " + e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
		}
		
		return "delete-result";
	}
	
	
	@RequestMapping(value = "/fibonacci", method = RequestMethod.GET)
	@ResponseBody
	public List<Integer> fibo(Locale locale, Model model, HttpSession session) {
		List<Integer> list = new ArrayList<Integer>();
		int i ;
		int a =0;
		int b =1;
		int f;
		list.add(a);
		list.add(b);
		for( i=0; i<100 ; i++ ) {
			f=a+b;
			a = b;
			b = f;
			list.add(f);
		}
		
		return list;
	}
	
	@RequestMapping(value = "/maxmin", method = RequestMethod.GET)
	@ResponseBody
	public String matriz(Locale locale, Model model, HttpSession session) {
		
		int[][] mat = {{23, 25, 78},{36, 74, 15},{52, 75, 35},{24, 22, 45}};
		
		int min = mat[0][0];
		int max = mat[0][0];
		for(int x = 0; x < mat.length; x++) {
			for(int j = 0; j < mat[x].length; j++) {
				if(mat[x][j] < min) {
					min = mat[x][j];
				}
				if(mat[x][j] > max){
					max = mat[x][j];
				}
			}
		}
		return "El menor : "+min + ", El mayor : "+max;
	}
}
