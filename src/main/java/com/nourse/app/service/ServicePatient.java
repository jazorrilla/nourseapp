package com.nourse.app.service;

import java.util.List;

import com.nourse.app.bean.NourseData;
import com.nourse.app.bean.Patient;
import com.nourse.app.bean.PatientModel;
import com.nourse.app.bean.SignsBean;

public interface ServicePatient {
	List<Patient> getPatient();
	String insertPatient(PatientModel patientModel) throws Exception;
	List<SignsBean> getSigns();
	String editPatient(PatientModel patientModel) throws Exception;
	String deletePatient(PatientModel patientModel) throws Exception;
	NourseData validateNourse(String user, String pass) throws Exception;
}
