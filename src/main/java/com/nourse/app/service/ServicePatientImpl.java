package com.nourse.app.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Service;

import com.nourse.app.bean.NourseData;
import com.nourse.app.bean.Patient;
import com.nourse.app.bean.PatientModel;
import com.nourse.app.bean.SignsBean;
import com.nourse.app.bean.SignsModel;
import com.nourse.app.doa.PatientDAO;


@Service
public class ServicePatientImpl implements ServicePatient{

	//private PatientDAO peDao;
	
	public ServicePatientImpl() {
		//peDao = new PatientDAO();
    }
	
	@Override
	public List<Patient> getPatient(){
		List<Patient> lista = new ArrayList<Patient>();
			
		PatientDAO peDao = new PatientDAO();
		lista = peDao.getPersonas();
		LocalDate localDate = LocalDate.now();
		for(Patient p : lista) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(p.getDateOfBirth());
			LocalDate dateBirth = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			p.setAge(""+Period.between(dateBirth, localDate).getYears());
			p.setSigns(peDao.getSigns(p.getIdPark()));
		}
		
		return lista;
	}
	
	@Override
	public String insertPatient(PatientModel patientModel) throws Exception {
		PatientDAO peDao = new PatientDAO();
		String result = peDao.getInsertaPatient(patientModel);
		if(result.equals("1")) {
			throw new Exception("An error occurred");
		}
		for(SignsModel s : patientModel.getListSigns()) {
			peDao.addSignValue(s);
		}
		return result;
	}
	
	@Override
	public List<SignsBean> getSigns(){
		PatientDAO peDao = new PatientDAO();
		List<SignsBean> signs = new ArrayList<SignsBean>();
		signs = peDao.getSigns();
		return signs;
	}
	
	
	@Override
	public String editPatient(PatientModel patientModel) throws Exception {
		PatientDAO peDao = new PatientDAO();
		String result = peDao.editPatient(patientModel);
		
		return result;
	}
	
	@Override
	public String deletePatient(PatientModel patientModel) throws Exception {
		PatientDAO peDao = new PatientDAO();
		String result = peDao.deletePatient(patientModel);
		
		return result;
	}
	
	@Override
	public NourseData validateNourse(String userName, String pass) throws Exception {
		PatientDAO peDao = new PatientDAO();
		NourseData nourseData = peDao.valNourse(userName, pass);
		if(nourseData == null) {
			throw new Exception("Invalid user or password");
		}
		return nourseData;
	}
	
}
