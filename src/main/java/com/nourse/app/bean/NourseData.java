package com.nourse.app.bean;

public class NourseData {
	 private Integer idNource;
	 private String name;
	 private String user;
	 private String pass;
	 private String userType;
	public Integer getIdNource() {
		return idNource;
	}
	public void setIdNource(Integer idNource) {
		this.idNource = idNource;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	
}
