package com.nourse.app.bean;

public class SignsBean {
	private Integer idSign;
	private String description;
	private Double minValue;
	private Double maxValue;
	
	public Integer getIdSign() {
		return idSign;
	}
	public void setIdSign(Integer idSign) {
		this.idSign = idSign;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getMinValue() {
		return minValue;
	}
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}
	public Double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}
}
