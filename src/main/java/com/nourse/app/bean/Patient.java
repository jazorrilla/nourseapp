package com.nourse.app.bean;

import java.util.Date;
import java.util.List;

public class Patient {
	private Integer idPatient;
	private String name;
	private String lastname;
	private String ci;
	private Date dateOfBirth;
	private String age;
	private String phoneNumber;
	private Integer idPark;
	private Date lastDate;
	private String aditionalInfo;
	private List<SignsModel> signs;
	public Integer getIdPatient() {
		return idPatient;
	}
	public void setIdPatient(Integer idPatient) {
		this.idPatient = idPatient;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Integer getIdPark() {
		return idPark;
	}
	public void setIdPark(Integer idPark) {
		this.idPark = idPark;
	}
	public Date getLastDate() {
		return lastDate;
	}
	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}
	public String getAditionalInfo() {
		return aditionalInfo;
	}
	public void setAditionalInfo(String aditionalInfo) {
		this.aditionalInfo = aditionalInfo;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public List<SignsModel> getSigns() {
		return signs;
	}
	public void setSigns(List<SignsModel> signs) {
		this.signs = signs;
	}
	@Override
	public String toString() {
		return "Patient [idPatient=" + idPatient + ", name=" + name + ", lastname=" + lastname + ", ci=" + ci + ", age="
				+ age + ", phoneNumber=" + phoneNumber + ", idPark=" + idPark + ", lastDate=" + lastDate
				+ ", aditionalInfo=" + aditionalInfo + "]";
	}
}
