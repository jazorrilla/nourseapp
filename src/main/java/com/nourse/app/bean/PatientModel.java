package com.nourse.app.bean;

import java.util.List;

public class PatientModel {
	private Integer idPatient;
	private Integer idPart;
	private String name;
	private String lastName;
	private String ci;
	private String dateBirth;
	private String phoneNumber;
	private String infoAditional;
	
	private List<SignsModel> listSigns;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getDateBirth() {
		return dateBirth;
	}
	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getInfoAditional() {
		return infoAditional;
	}
	public void setInfoAditional(String infoAditional) {
		this.infoAditional = infoAditional;
	}
	public List<SignsModel> getListSigns() {
		return listSigns;
	}
	public void setListSigns(List<SignsModel> listSigns) {
		this.listSigns = listSigns;
	}
	public Integer getIdPatient() {
		return idPatient;
	}
	public void setIdPatient(Integer idPatient) {
		this.idPatient = idPatient;
	}
	public Integer getIdPart() {
		return idPart;
	}
	public void setIdPart(Integer idPart) {
		this.idPart = idPart;
	}
	
}
