package com.nourse.app.bean;

public class SignsModel {
	private String value;
	private Double valueD;
	private SignsBean signsBean;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public SignsBean getSignsBean() {
		return signsBean;
	}
	public void setSignsBean(SignsBean signsBean) {
		this.signsBean = signsBean;
	}
	public Double getValueD() {
		return valueD;
	}
	public void setValueD(Double valueD) {
		this.valueD = valueD;
	}
	
}
