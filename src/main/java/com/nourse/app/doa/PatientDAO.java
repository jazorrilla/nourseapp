package com.nourse.app.doa;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.nourse.app.bean.NourseData;
import com.nourse.app.bean.Patient;
import com.nourse.app.bean.PatientModel;
import com.nourse.app.bean.SignsBean;
import com.nourse.app.bean.SignsModel;
import com.nourse.app.conection.ConexionSQL;


public class PatientDAO{
	
	public List<Patient> getPersonas(){
		List<Patient> patients = new ArrayList<Patient>();
		int i=0;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		       String sSQL="select " + 
		       		"p.id_patient, p.name, p.lastname, p.ci, p.date_of_birth, p.phone_number, " + 
		       		"mp.id_park, mp.last_date, mp.aditional_info " + 
		       		"from patient p, medical_part mp " + 
		       		"where p.id_patient = mp.id_patient";
		       try {
		           Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	Patient p = new Patient();
		            
		            p.setIdPatient(rs.getInt(1));
		            p.setName(rs.getString(2));
		            p.setLastname(rs.getString(3));
		            p.setCi(rs.getString(4));
		            p.setDateOfBirth(new java.util.Date(rs.getDate(5).getTime()));
		            p.setPhoneNumber(rs.getString(6));
		            p.setIdPark(rs.getInt(7));
		            p.setLastDate(new java.util.Date(rs.getDate(8).getTime()));
		            p.setAditionalInfo(rs.getString(9));
		            
		            patients.add(p);
		           }
		       } 
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       return patients;
	}
		
	public String getInsertaPatient(PatientModel p) throws Exception{
		String retorno = null;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       if(valCi(p.getCi()) > 0) {
		    	   throw new Exception("Ya existe registro");
		       }
		       
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "select public.insertPatient('"+p.getName()+"', '"+p.getLastName()+"', '"+p.getCi()+"', '"+p.getPhoneNumber()+"', to_date('"+p.getDateBirth()+"', 'DD/MM/YYYY'),'"+p.getInfoAditional()+"');";
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   retorno = rs.getString(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       return retorno;
	}
	
	public String addSignValue(SignsModel s) throws Exception{
		String retorno = null;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = null;
		       if((s.getSignsBean().getMinValue() != null && s.getSignsBean().getMinValue() > 0.0) && (s.getSignsBean().getMaxValue() != null && s.getSignsBean().getMaxValue() > 0.0)) {
		    	   sSQL = "INSERT INTO public.sign_part_value( id_part, id_vital_sing, value) VALUES ((select max(id_park) from public.medical_part),"+s.getSignsBean().getIdSign()+", "+s.getValue()+");";
		       }else {
		    	   sSQL = "INSERT INTO public.sign_part_value( id_part, id_vital_sing, val) VALUES ((select max(id_park) from public.medical_part),"+s.getSignsBean().getIdSign()+", '"+s.getValue()+"');";
		       }
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   retorno = rs.getString(1);
		           }
		           
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       return retorno;
	}
	
	public List<SignsBean> getSigns(){
		List<SignsBean> listSings = new ArrayList<SignsBean>();
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "select id_sign,description,min_value,max_value from public.vital_signs";
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   SignsBean sg = new SignsBean();
		        	   sg.setIdSign(rs.getInt(1));
		        	   sg.setDescription(rs.getString(2));
		        	   sg.setMinValue(rs.getDouble(3));
		        	   sg.setMaxValue(rs.getDouble(4));
		        	   listSings.add(sg);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       return listSings;
	}
	
	public int valCi(String ci){
		int retorno = 0;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "select count(*) from patient where ci = '"+ci+"';";
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   retorno = rs.getInt(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       return retorno;
	}
	
	public List<SignsModel> getSigns(int idPart){
		List<SignsModel> signs = new ArrayList<SignsModel>();
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "SELECT sv.id_part, sv.id_vital_sing, sv.value, sv.val, vs.description, vs.min_value, vs.max_value FROM public.sign_part_value sv, public.vital_signs vs where sv.id_vital_sing = vs.id_sign and sv.id_part = "+idPart;
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   SignsModel signsModel = new SignsModel();
		        	   SignsBean signsBean = new SignsBean();
		        	   signsBean.setIdSign(rs.getInt(2));
		        	   signsBean.setDescription(rs.getString(5));
		        	   signsBean.setMinValue(rs.getDouble(6));
		        	   signsBean.setMaxValue(rs.getDouble(7));
		        	   
				       if((signsBean.getMinValue() != null && signsBean.getMinValue() > 0.0) && (signsBean.getMaxValue() != null && signsBean.getMaxValue() > 0.0)) {
				    	   signsModel.setValue(""+rs.getDouble(3));
				    	   signsModel.setValueD(rs.getDouble(3));
				       }else {
				    	   signsModel.setValue(rs.getString(4));
				    	   signsBean.setMinValue(null);
			        	   signsBean.setMaxValue(null);
				       }
				       signsModel.setSignsBean(signsBean);
		        	   
				       
		        	   signs.add(signsModel);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       return signs;
	}

	
	public String editPatient(PatientModel p) throws Exception{
		String retorno = null;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       /*if(valCi(p.getCi()) > 0) {
		    	   throw new Exception("Ya existe registro");
		       }*/
		       
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "UPDATE public.patient SET name='"+p.getName()+"', lastname='"+p.getLastName()+"', ci='"+p.getCi()+"', phone_number='"+p.getPhoneNumber()+"', date_of_birth=to_date('"+p.getDateBirth()+"', 'DD/MM/YYYY') WHERE id_patient = "+p.getIdPatient()+";";
		       String sSQL2 = "UPDATE public.medical_part SET last_date=current_timestamp, aditional_info='"+p.getInfoAditional()+"' WHERE id_park = "+p.getIdPart()+";";
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   retorno = rs.getString(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL2);
		           while(rs.next()){
		        	   retorno = rs.getString(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       for(SignsModel sg : p.getListSigns()){
		    	   
		    	   String newSql = null;
		    	   
		    	   if((sg.getSignsBean().getMinValue() != null && sg.getSignsBean().getMinValue() > 0.0) && (sg.getSignsBean().getMaxValue() != null && sg.getSignsBean().getMaxValue() > 0.0)) {
		    		   newSql = "UPDATE public.sign_part_value SET value="+sg.getValue()+" WHERE id_part = "+p.getIdPart()+" and id_vital_sing = "+sg.getSignsBean().getIdSign()+";";
			       }else {
			    	   newSql = "UPDATE public.sign_part_value SET val='"+sg.getValue()+"' WHERE id_part = "+p.getIdPart()+" and id_vital_sing = "+sg.getSignsBean().getIdSign()+";";
			       }
		    	   
			       try {
			    	   Statement st = cn.createStatement();
			           ResultSet rs = st.executeQuery(newSql);
			           while(rs.next()){
			        	   retorno = rs.getString(1);
			           }
			       }
			       catch (SQLException ex) 
			       {
			           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
			       }
		    	   
		       }
		       
		       return retorno;
	}
	
	public String deletePatient(PatientModel p) throws Exception{
		String retorno = null;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       /*if(valCi(p.getCi()) > 0) {
		    	   throw new Exception("Ya existe registro");
		       }*/
		       
		       for(SignsModel sg : p.getListSigns()){
		    	   
		    	   String newSql = null;
		    	   newSql = "DELETE FROM public.sign_part_value WHERE id_part = "+p.getIdPart()+" and id_vital_sing = "+sg.getSignsBean().getIdSign()+";";
			       		    	   
			       try {
			    	   Statement st = cn.createStatement();
			           ResultSet rs = st.executeQuery(newSql);
			           while(rs.next()){
			        	   retorno = rs.getString(1);
			           }
			       }
			       catch (SQLException ex) 
			       {
			           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
			       }
		    	   
		       }
		       
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "DELETE FROM public.patient WHERE id_patient = "+p.getIdPatient()+";";
		       String sSQL2 = "DELETE FROM public.medical_part WHERE id_park = "+p.getIdPart()+";";
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL2);
		           while(rs.next()){
		        	   retorno = rs.getString(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   retorno = rs.getString(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       
		       
		       return retorno;
	}
	
	public NourseData valNourse(String userName, String pass){
		NourseData nourceData = new NourseData();
		int retorno = 0;
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		      
		       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
		       String sSQL = "select count(*) FROM public.nourse where user_name = '"+userName+"' and pass = '"+pass+"';";
		       try {
		    	   Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		        	   retorno = rs.getInt(1);
		           }
		       }
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       
		       if(retorno > 0) {
		    	   
		    	 //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
			       String sSQL2 = "select id_nourse, user_name, pass, name, user_type FROM public.nourse where user_name = '"+userName+"' and pass = '"+pass+"';";
			       try {
			    	   Statement st = cn.createStatement();
			           ResultSet rs = st.executeQuery(sSQL2);
			           while(rs.next()){
			        	   nourceData.setIdNource(rs.getInt(1));
			        	   nourceData.setName(rs.getString(4));
			        	   nourceData.setPass(rs.getString(3));
			        	   nourceData.setUser(rs.getString(2));
			        	   nourceData.setUserType(rs.getString(5));
			           }
			       }
			       catch (SQLException ex) 
			       {
			           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
			       }
			       
		    	   
		       }else {
		    	   
		    	   return null;
		    	   
		       }
		       
		       return nourceData;
	}
		
	/*
public String getUpdatePass(String usuario, String passnew){
	String login = "S";
	int i=0;
	ConexionSQL mySQL = new ConexionSQL();
	       Connection cn = mySQL.conectar();
	       //String sSQL="SELECT public.updatepass('"+usuario+"','"+passactual+"','"+passnew+"','"+passnewconfir+"');";
	       String sSQL = "Update public.usuario set contrasena = '"+passnew+"' where usuario = '"+usuario+"';";
	       try {
	    	   Statement st = cn.createStatement();
	           ResultSet rs = st.executeQuery(sSQL);
	           while(rs.next()){
	        	   login = rs.getString(1);
	        	  
	           }
	       }
	       catch (SQLException ex) 
	       {
	           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
	       }
	       return login;
}
public String getInsertaSanatorio(String sanatorio, String direccion, String ciudad, String telefono){
	String retorno = null;
	int i=0;
	ConexionSQL mySQL = new ConexionSQL();
	       Connection cn = mySQL.conectar();
	      
	       //ByteArrayInputStream bt = new ByteArrayInputStream(imagen, imagen.length);
	       String sSQL = "insert into public.hospital(id_hospital, nombre, direccion, ciudad, telefono, estado) values(NEXTVAL('hospital_id_hospital_seq'),'"+sanatorio+"','"+direccion+"','"+ciudad+"','"+telefono+"','A');";
	       //String sSQL="SELECT public.f_inserta_imagen("+bt,imagen.length+",'"+newImagen.getDescripcion()+"',"+idHistorial+","+idHospital+","+idPaciente+",'"+newImagen.getCi_paciente()+"','"+newImagen.getReg_medico()+"');";
	       try {
	    	   Statement st = cn.createStatement();
	           ResultSet rs = st.executeQuery(sSQL);
	           while(rs.next()){
	        	   retorno = rs.getString(1);
	        	   
	           }
	       }
	       catch (SQLException ex) 
	       {
	           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
	       }
	       
	       return "OK";
}*/

}
